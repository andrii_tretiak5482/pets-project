## BitBucket
https://bitbucket.org/andrii_tretiak5482/pets-project/src/main/

## Project version branches

1. ### main

this version contains all functionality described in the provided document. In current version I implemented filtering
and pagination by getting all data from JSON-Server and made all operation in frontEnd part through Angular features;

2. ### filters_set_from_url

all filters and pagination done by JSON-Server Endpoint which server provide with saving all filters data in URL;

3. ### filters_set_from_subject

the same as filters_set_from_url but filters info save in subjects;

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4300/`. The app will automatically reload if you change
any of the source files.

## Running json-server

1. npm install json-server --save-dev (`https://github.com/typicode/json-server`);
2. in terminal use command :  json-server db.json --routes routes.json;
