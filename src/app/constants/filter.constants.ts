export const filters = {
    GENDER:  ['All', 'Female', 'Male'],
    TYPE_PETS: ['All', 'Cat', 'Dog']
}