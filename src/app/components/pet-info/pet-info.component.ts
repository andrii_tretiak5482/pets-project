import {Component, OnInit} from '@angular/core';
import {PetServiceService} from "../../service/pet-service.service";
import {Subject} from "rxjs";
import {PetInterface} from "../../interfaces/pet-interface";
import {ActivatedRoute} from "@angular/router";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from "@angular/material/snack-bar";
import {takeUntil} from "rxjs/operators";
import { Location } from '@angular/common'

@Component({
    selector: 'ss-pet-info',
    templateUrl: './pet-info.component.html',
    styleUrls: ['./pet-info.component.scss']
})
export class PetInfoComponent implements OnInit {
    destroy: Subject<boolean> = new Subject<boolean>();
    pet: PetInterface;
    petId: string;
    photoArr = [];
    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    private destroy$ = new Subject();

    constructor(private petsService: PetServiceService,
                private activatedRoute: ActivatedRoute,
                private _snackBar: MatSnackBar,
                private location: Location) {

    }

    ngOnInit() {
        const petId = this.activatedRoute.snapshot.paramMap.get('id');
        this.getPetById(petId);
    }

    getPetById(id: string | null) {
        this.petsService.getPetById(id).pipe(takeUntil(this.destroy$)).subscribe(res => {
            this.pet = res[0];
            this.createPhotoArr(this.pet);
        });
    }

    createPhotoArr(pet: PetInterface) {
        this.photoArr.push(pet.photo as never);
        pet.additionalPhoto.forEach(item => {
            this.photoArr.push(item);
        })
    }

    adoptPet(id: number) {
        if (!this.pet.isAdopt) {
            this.pet.isAdopt = true;
            this.petsService.updatePet(id, this.pet).pipe(takeUntil(this.destroy$)).subscribe();
            this._snackBar.open('Thanks you successfully adopt', 'Ok', {
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
                duration: 2000
            });
        }

    }

    back(){
        this.location.back()
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
