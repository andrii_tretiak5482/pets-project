import {Component, OnDestroy} from '@angular/core';
import {PetServiceService} from "../../service/pet-service.service";
import {MyType} from "../../interfaces/pet-interface";
import {Subject} from "rxjs";
import {filters} from "../../constants/filter.constants";

@Component({
    selector: 'filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnDestroy{
    typeArr = filters.TYPE_PETS;
    genderArr = filters.GENDER;
    typePets = filters.TYPE_PETS[0];
    gender = filters.GENDER[0];
    private destroy$ = new Subject();

    constructor(public petsService: PetServiceService) {
    }

    filterPets(option: string, isGender: boolean) {
        const queryParams: MyType = {
            page: 0,
        }
        queryParams[isGender ? 'gender' : 'type'] = option;
        this.petsService.filters.next(queryParams);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
