import {Component, OnDestroy} from '@angular/core';
import {Subject} from "rxjs";
import {PetServiceService} from "../../../service/pet-service.service";

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnDestroy {
    private destroy$ = new Subject();

    constructor(private petsService: PetServiceService) {
    }

    search(value: string) {
        this.petsService.filters.next({searchValue: value, page: 0});
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
