import {Component, OnDestroy, OnInit} from '@angular/core';
import {ParamsFiltering, PetInterface} from "../../interfaces/pet-interface";
import {ActivatedRoute, Router} from "@angular/router";
import {PetServiceService} from "../../service/pet-service.service";
import {Subject} from "rxjs";
import {map} from "rxjs/operators";

@Component({
    selector: 'ss-pets',
    templateUrl: './pets.component.html',
    styleUrls: ['./pets.component.scss']
})
export class PetsComponent implements OnInit, OnDestroy {
    pets: PetInterface[] | null | undefined | PetInterface;
    count: number | null;
    queryParams: ParamsFiltering = {
        page: 0,
        type: 'All',
        searchValue: '',
        gender: 'All'
    }
    private destroy$ = new Subject();

    constructor(private router: Router,
                private route: ActivatedRoute,
                private petsService: PetServiceService) {
    }

    ngOnInit() {
        this.getPets(this.queryParams);
        this.petsService.filters.subscribe(params => {
            this.queryParams = {...this.queryParams, ...params};
            this.getPets(this.queryParams);
        });
    }

    filterByType(pets: PetInterface[], type: string | undefined) {
        return pets.filter(item => item.type === type?.toLowerCase());
    }

    filterByGender(pets: PetInterface[], gender: string | undefined) {
        return pets.filter(item => item.gender === gender);
    }

    filterByGenderAndType(pets: PetInterface[], paramsFiltering: ParamsFiltering) {
        return pets.filter(item => item.type === paramsFiltering.type?.toLowerCase() && item.gender === paramsFiltering.gender);
    }

    filterBySearchInput(pets: PetInterface[], paramsFiltering: ParamsFiltering) {
        return pets.filter(item => item.name.toLowerCase().includes(paramsFiltering.searchValue?.toLowerCase() as string) || item.breed.toLowerCase().includes(paramsFiltering.searchValue?.toLowerCase() as string));
    }

    getPets(paramsFiltering: ParamsFiltering) {
        this.petsService.getPets()
            .pipe(map(pets => {
                if (paramsFiltering.type !== 'All' && paramsFiltering.gender === 'All') {
                    pets = this.filterByType(pets, paramsFiltering.type);
                }
                if (paramsFiltering.type === 'All' && paramsFiltering.gender !== 'All') {
                    pets = this.filterByGender(pets, paramsFiltering.gender);
                }
                if (paramsFiltering.type !== 'All' && paramsFiltering.gender !== 'All') {
                    pets = this.filterByGenderAndType(pets, paramsFiltering);
                }
                if (paramsFiltering.searchValue) {
                    pets = this.filterBySearchInput(pets, paramsFiltering);
                }
                return pets;
            }))
            .subscribe((item) => {
                this.pets = item;
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
