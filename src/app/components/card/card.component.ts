import {
    Component, Input, OnInit
} from '@angular/core';
import {PetInterface} from "../../interfaces/pet-interface";
import {Router} from "@angular/router";
import {PetServiceService} from "../../service/pet-service.service";

@Component({
    selector: 'card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit{

    @Input() pets: PetInterface[];
    @Input() length: number | null;
    page: number | undefined = 0;
    constructor(
        private router: Router,
        public petsService: PetServiceService
    ) {
    }

    ngOnInit(): void {
        this.petsService.filters.subscribe(params => {
            this.page = params.page;
        });
    }

    redirectToPetInfo(pet: PetInterface) {
        this.router?.navigate(['pets/pet', {id: pet.id}]);
    }
}