import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { CardComponent } from './components/card/card.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {FlexModule} from "@angular/flex-layout";
import { FiltersComponent } from './components/header/filters.component';
import {RouterModule} from "@angular/router";
import {MatPaginatorModule} from "@angular/material/paginator";
import {NgxPaginationModule} from "ngx-pagination";
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatRadioModule} from "@angular/material/radio";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { SearchComponent } from './components/header/search/search.component';
import {MatIconModule} from "@angular/material/icon";
import { PetInfoComponent } from './components/pet-info/pet-info.component';
import { PetsComponent } from './components/pets/pets.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";

@NgModule({
    declarations: [
        AppComponent,
        CardComponent,
        FiltersComponent,
        SearchComponent,
        PetInfoComponent,
        PetsComponent
    ],
    imports: [
        FormsModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        AppRoutingModule,
        RouterModule,
        HttpClientModule,
        BrowserModule,
        MatCardModule,
        MatButtonModule,
        FlexModule,
        NgxPaginationModule,
        MatPaginatorModule,
        BrowserAnimationsModule,
        MatRadioModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatIconModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
