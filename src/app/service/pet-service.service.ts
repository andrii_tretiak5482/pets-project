import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MyType, ParamsFiltering, PetInterface} from "../interfaces/pet-interface";
import {Observable, ReplaySubject} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PetServiceService {
    filters: ReplaySubject<ParamsFiltering | MyType> = new ReplaySubject<ParamsFiltering | MyType>(1);
    basedUrl = 'http://localhost:3000/';

    constructor(private http: HttpClient) {
    }

    getPets(): Observable<PetInterface[]> {
        return this.http.get<PetInterface[]>(
            this.basedUrl +
            `pets`);
    }

    getPetById(id: string | null): Observable<PetInterface[]> {
        return this.http.get<PetInterface[]>(this.basedUrl + `pets?id=${id}`);
    }

    updatePet(id: number, pet: PetInterface): Observable<PetInterface> {
        return this.http.put<PetInterface>(this.basedUrl + `pets/${id}`, pet);
    }
}
