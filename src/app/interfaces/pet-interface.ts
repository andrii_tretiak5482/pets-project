export interface PetInterface {
    id: number,
    breed: string,
    name: string,
    gender: string
    age: Age,
    type: string,
    isAdopt: boolean,
    description: string,
    medicalNotes: boolean,
    photo: string,
    location: string
    additionalPhoto: []
}

export interface Age {
    year: number,
    month: number
}

export interface ParamsFiltering {
    page?: number,
    pageSize?: number | string,
    searchValue?: string,
    type?: string,
    gender?: string
}

export interface AdoptInfo {
    id: number,
    isAdopt: boolean
}

export interface MyType {
    page: number,
    [key: string]: any
}