import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PetInfoComponent} from "./components/pet-info/pet-info.component";
import {PetsComponent} from "./components/pets/pets.component";

const routes: Routes = [
  {
    path: '',
    redirectTo:'/pets',
    pathMatch:'full',
  },
  {
    path: 'pets',
    component: PetsComponent
  },
  {
    path: 'pets/pet',
    component: PetInfoComponent
  },
  {
    path: '**',
    redirectTo: '/pets'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
